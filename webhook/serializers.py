
from .models  import WebHook
from rest_framework import serializers

class WebHookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta: 
        model = WebHook
        fields = ['id', 'action', 'url', 'response', 'status_field']