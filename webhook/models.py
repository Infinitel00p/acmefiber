from django.db import models

# Create your models here.

class WebHook(models.Model):
    action = models.CharField(max_length=80, db_index=True)
    url = models.CharField(max_length=80)
    response = models.PositiveSmallIntegerField(default=200)
    status_field = models.BooleanField(default=True)

