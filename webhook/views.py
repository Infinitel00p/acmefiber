from django.shortcuts import render

# Create your views here.

from .models import WebHook
from rest_framework import viewsets
from .serializers import WebHookSerializer

class WebHookSet(viewsets.ModelViewSet):
    # lookup_field = 'action'
    queryset = WebHook.objects.all()
    serializer_class = WebHookSerializer